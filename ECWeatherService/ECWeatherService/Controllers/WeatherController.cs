﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ECWeatherService.Models;

namespace ECWeatherService.Controllers
{
    public class WeatherController : ApiController
    {
        private WeatherData[] weatherData =
        {
            new WeatherData { Day = 1, Hour = 5, RainChance =  45 },
            new WeatherData { Day = 1, Hour = 6, RainChance =  26 },
            new WeatherData { Day = 1, Hour = 7, RainChance =  9 },
            new WeatherData { Day = 1, Hour = 8, RainChance =  88 },
            new WeatherData { Day = 1, Hour = 9, RainChance =  94 },
            new WeatherData { Day = 1, Hour = 10, RainChance = 65  },
            new WeatherData { Day = 2, Hour = 5, RainChance =  14 },
            new WeatherData { Day = 2, Hour = 6, RainChance =  66 },
            new WeatherData { Day = 2, Hour = 7, RainChance =  41 },
            new WeatherData { Day = 2, Hour = 8, RainChance =  37 },
            new WeatherData { Day = 2, Hour = 9, RainChance =  17 },
            new WeatherData { Day = 2, Hour = 10, RainChance =  1 },
            new WeatherData { Day = 3, Hour = 5, RainChance =  80 },
            new WeatherData { Day = 3, Hour = 6, RainChance =  21 },
            new WeatherData { Day = 3, Hour = 7, RainChance =  91 },
            new WeatherData { Day = 3, Hour = 8, RainChance =  94 },
            new WeatherData { Day = 3, Hour = 9, RainChance =  66 },
            new WeatherData { Day = 3, Hour = 10, RainChance =  96 },
        };

        public IEnumerable<WeatherData> GetWeatherData()
        {
            return weatherData;
        }

    }
}
