﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECMobileAPI.Models
{
    public class WeatherData
    {
        public int Day { get; set; }
        public int Hour { get; set; }
        public int RainChance { get; set; }
    }
}