﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECMobileAPI.Models
{
    public class ArtistData
    {
        public string Stage { get; set; }
        public int Day { get; set; }
        public int Hour { get; set; }
        public int RainChance { get; set; }
    }
}