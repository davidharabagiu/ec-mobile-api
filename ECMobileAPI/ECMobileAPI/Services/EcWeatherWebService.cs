﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECMobileAPI.Models;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ECMobileAPI.Services
{
    public class EcWeatherWebService
    {
        private HttpClient client;

        public EcWeatherWebService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:5254/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public IEnumerable<WeatherData> GetWeatherData()
        {
            return GetWeatherDataAsync("api/weather").Result;
        }

        private async Task<IEnumerable<WeatherData>> GetWeatherDataAsync(string path)
        {
            IEnumerable<WeatherData> weatherData = null;
            HttpResponseMessage response = await client.GetAsync(path).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                weatherData = await response.Content.ReadAsAsync<IEnumerable<WeatherData>>();
            }
            return weatherData;
        }
    }
}