﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECMobileAPI.Models;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ECMobileAPI.Services
{
    public class EcArtistsWebService : IArtistsService
    {
        private HttpClient client;

        public EcArtistsWebService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:2528/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public IEnumerable<Artist> GetArtistData()
        {
            return GetArtistsAsync("api/artists").Result;
        }

        public void SetNext(IArtistsService next)
        {
            throw new NotImplementedException();
        }

        private async Task<IEnumerable<Artist>> GetArtistsAsync(string path)
        {
            IEnumerable<Artist> artists = null;
            HttpResponseMessage response = await client.GetAsync(path).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                artists = await response.Content.ReadAsAsync<IEnumerable<Artist>>();
            }
            return artists;
        }
    }
}