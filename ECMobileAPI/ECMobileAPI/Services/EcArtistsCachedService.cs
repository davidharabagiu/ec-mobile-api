﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECMobileAPI.Models;

namespace ECMobileAPI.Services
{
    public class EcArtistsCachedService : IArtistsService
    {
        private static DateTime lastUpdate;
        private static IEnumerable<Artist> artistData;
        private IArtistsService webService;

        public void SetNext(IArtistsService next)
        {
            webService = next;
        }

        public IEnumerable<Artist> GetArtistData()
        {
            if (IsExpired())
            {
                lastUpdate = DateTime.Now;
                SetArtistData();
            }

            return artistData;
        }

        private void SetArtistData()
        {
            if (webService != null)
            {
                artistData = webService.GetArtistData();
            }
        }

        private bool IsExpired()
        {
            DateTime currentUpdate = DateTime.Now;
            return artistData == null || currentUpdate.Subtract(lastUpdate).TotalSeconds > 30;
        }
    }
}