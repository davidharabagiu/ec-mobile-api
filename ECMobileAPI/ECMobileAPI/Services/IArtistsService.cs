﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECMobileAPI.Models;

namespace ECMobileAPI.Services
{
    public interface IArtistsService
    {
        IEnumerable<Artist> GetArtistData();
        void SetNext(IArtistsService next);
    }
}
