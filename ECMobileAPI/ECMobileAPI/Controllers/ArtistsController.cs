﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ECMobileAPI.Models;
using ECMobileAPI.Services;

namespace ECMobileAPI.Controllers
{
    public class ArtistsController : ApiController
    {
        private IArtistsService artistsService;
        private EcWeatherWebService weatherService;

        protected ArtistsController()
            : base()
        {
            artistsService = new EcArtistsCachedService();
            artistsService.SetNext(new EcArtistsWebService());
            weatherService = new EcWeatherWebService();
        }

        public IEnumerable<Artist> GetArtists()
        {
            return artistsService.GetArtistData();
        }

        public IEnumerable<Artist> GetArtists(string stage)
        {
            return artistsService.GetArtistData().Where(artist => artist.Stage == stage);
        }

        public IEnumerable<Artist> GetArtists(int day, int hour)
        {
            return artistsService.GetArtistData().Where(artist => artist.Day == day && artist.Hour == hour);
        }

        public ArtistData GetWeather(string artistName)
        {
            var artistData = artistsService.GetArtistData().Where(a => a.Name == artistName);
            var artistEnumerator = artistData.GetEnumerator();
            artistEnumerator.MoveNext();
            Artist artist = artistEnumerator.Current;
            var weatherData = weatherService.GetWeatherData().Where(w => w.Day == artist.Day && w.Hour == artist.Hour);
            var weatherEnumerator = weatherData.GetEnumerator();
            weatherEnumerator.MoveNext();
            return new ArtistData { Stage = artist.Stage, Day = artist.Day, Hour = artist.Hour, RainChance = weatherEnumerator.Current.RainChance };
        }
    }
}
