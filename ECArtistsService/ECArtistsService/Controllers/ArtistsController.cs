﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ECArtistsService.Models;

namespace ECArtistsService.Controllers
{
    public class ArtistsController : ApiController
    {
        private Artist[] artists = new Artist[]
        {
            new Artist { Name = "Jessie J", Stage = "Main", Day = 1, Hour = 5 },
            new Artist { Name = "Cancer Bats", Stage = "Main", Day = 1, Hour = 8 },
            new Artist { Name = "JP Cooper", Stage = "Main", Day = 1, Hour = 10 },
            new Artist { Name = "Raresh", Stage = "Main", Day = 2, Hour = 5 },
            new Artist { Name = "Nothing But Thieves", Stage = "Main", Day = 2, Hour = 10 },
            new Artist { Name = "Subcarpati", Stage = "Main", Day = 3, Hour = 9 },
            new Artist { Name = "London Grammar", Stage = "Main", Day = 3, Hour = 10 },
            new Artist { Name = "Damian Marley", Stage = "Red Bull", Day = 1, Hour = 5 },
            new Artist { Name = "Excision", Stage = "Red Bull", Day = 1, Hour = 6 },
            new Artist { Name = "San Holo", Stage = "Red Bull", Day = 2, Hour = 6 },
            new Artist { Name = "Wolf Alice", Stage = "Red Bull", Day = 2, Hour = 7 },
            new Artist { Name = "Elrow", Stage = "Red Bull", Day = 2, Hour = 10 },
            new Artist { Name = "Vita de Vie", Stage = "Red Bull", Day = 3, Hour = 10 },
            new Artist { Name = "Mura Masa", Stage = "BT", Day = 1, Hour = 5 },
            new Artist { Name = "Richie Hawtin", Stage = "BT", Day = 1, Hour = 6 },
            new Artist { Name = "High Contrast", Stage = "BT", Day = 2, Hour = 6 },
            new Artist { Name = "Son Lux", Stage = "BT", Day = 2, Hour = 7 },
            new Artist { Name = "Mum", Stage = "BT", Day = 2, Hour = 9 },
            new Artist { Name = "Alison Wonderland", Stage = "BT", Day = 2, Hour = 10 },
        };

        public IEnumerable<Artist> GetAllArtists()
        {
            return artists;
        }
    }
}
