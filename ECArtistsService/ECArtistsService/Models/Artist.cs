﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECArtistsService.Models
{
    public class Artist
    {
        public string Name { get; set; }
        public string Stage { get; set; }
        public int Day { get; set; }
        public int Hour { get; set; }
    }
}